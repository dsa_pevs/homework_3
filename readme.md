FIFO inventár zbožia na sklade:
-------------------------------

`Napíšte interaktívny program v jazyku C++, ktorý bude nástrojom na spracovanie inventára zbožia v sklade pre danú dodavateľskú firmu.Budete používať FIFO metódu na spracovanie tohto inventára. Podľa nej firma bude mať v sklade vždy iba to zbožie, ktoré bolo nakúpené naposledy, t.j. staršie položky zbožia sa budú predávať ako prvé. FIFO metóda na spracovanie zbožia v sklade sa používa najmä v čase padajúcich cien. Každý druh zbožia má svoju FIFO frontu. Podľa tejto metódy najdrahšie položky daného zbožia sa nachádzajú vo fronte na začiatku zatiaľ čo najlacnejšie položky sú na konci frontu. Keď sa uskutoční nejaký predaj zbožia, tak sa predajú položky zbožia zo začiatku frontu. Keď sa naopak nakúpia nové položky zbožia tak sa pridajú na koniec daného frontu. Tento program bude používať 3 fronty na simuláciu inventára zbožia typu 1, 2 a 3. Cieľom programu je spracovať nákupy a predaje zbožia podľa požiadavok používateľa.`


VSTUP:
------
Vstup do programu sa skladá z dvoch častí: z textového súboru `inventar.txt`, ktorý sa má načítať programom a zo série vstupov z klávesnice.

~~~
1) Textový súbor `inventar.txt` má následujúci formát ( **číslo_položky typ_transakcie počet_položiek cena_položky** ):

    > 1K 12 35.50
    > 3K 31 24.50
    > 1P 18 42.00
    > 2P 15 26.50

    Prvý riadok reprezentuje nákup položky zbožia typu 1 v počte 12 kusov za cenu 35.50

    Štvrtý riadok reprezentuje: predaj položky zbožia typu 2 v počte 15 kusov za cenu 26.50

2) Dotazy budú zadávané interaktívne z klávesnice a ich je určený následovnou ponukou (menu):

    > <i>nventar ......... vypíš inventár danej položky
    > <k>up ......... nákup k kusov danej položky
    > <p>redaj ......... predaj k kusov danej položky
    > <r>ead ......... načítaj inventár zo súboru
    > <s>tatistics ......... vypíš štatistiku zbožia
    > <w>rite ......... zapíš inventár do súboru
    > <e>xit ......... ukonči daný program.

V každom jednom dotaze užívateľ zadá príslušné údaje potrebné pre jeho prevedenie a program si ich uchová v pamäti pomocou AUT.
~~~

VYSTUP:
-------
`Program bude vypisovať na monitor:`
~~~
Zadaj svoju volbu: s

    > Item 1
    > 2 units @ 35.50 SKK
    > 10 units @ 28.50 SKK
    > Sumar 361.00 SKK

    > Item 2
    > 10 units @ 35.50 SKK
    > 1 units @ 25.00 SKK
    > Sumar 380.00 SKK
~~~

ZAOBCHÁDZANIE S CHYBAMI:
------------------------
~~~
• Ak množstvo tovaru, ktoré sa má v danom okamihu predať je väčšie ako množstvo v danej fronte, potom danú transakciu zrušte.
• Ostatné chyby pri vstupe ignorujte.
• Program sa nesmie zrutiť.
~~~

DÁTOVÉ ŠTRUKTÚRY:
-----------------

Rada položiek daného zbožia musí byť implementovaná ako fronta, t.j. ako abstraktný údajový typ (AUT) spolu s príslušnými operáciami `použitím tried` v C++. Naviac musíte použiť zreťazený (spájaný) zoznam pri jej implementácii.Jeden jeho uzol môže uchovávať iba tovar danej pevnej kúpnej ceny. Používajte dôsledne objektovo orientovaný prístup v C++.

ODOVZAJTE:
----------

~~~

1. špecifikáciu AUT v príslušných hlavičkových súboroch a odladený zdrojový kód ich implementácie
2. hlavný program `main.cpp` určený na testovanie daného AUT
3. dátový súbor: `inventar.txt` vytlačený na papieri
4. osobná prezentácia programu na cvičení

~~~
